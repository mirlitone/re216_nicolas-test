#!/usr/bin/env bash
#given a bitbucket repo, create a fork, add teachers as viewers and get the source on your computer.

echo "welcome to re216, we hope you'll have fun."

echo "If you don't have a bitbucket account, please create one now."
echo "Enter your bitbucket user name"
read login
echo "Great. Now enter your bitbucket password"
read -s password
echo "Good. please enter enseirb usernames for every student in the group, separated with a dash (eg: bgates-rstallman)"
read unames
http_code=$(curl --silent --write-out '%{http_code}' -X POST -u $login:$password   https://api.bitbucket.org/1.0/repositories/re216/re216/fork   --data "name=re216_$unames" 2>/dev/null -o re216-curl.log)
if [ "$http_code" != "200" ];
    then echo "something went wrong when creating the fork, please check re216.log"a
    exit -2
fi
echo "fork performed correclty, setting repository properties."
sleep 10 #waiting for bitbucket to allow us performing repo update. It might take a few seconds, so we wait

http_code=$(curl --silent --write-out '%{http_code}' -X PUT -u $login:$password   https://api.bitbucket.org/1.0/repositories/$login/re216_$unames --data 'is_private=true&description=$unames' 2>>/dev/null -o re216-curl.log)
if [ "$http_code" != "200" ];
    then echo "something went wrong when setting the repo properties"
    exit -3
fi

echo "setting up your local git"
git config --global user.name "$unames"


#giving access to the code to TA
curl -s --request PUT --user $login:$password https://bitbucket.org/api/1.0/privileges/$login/re216_$unames/nherbaut-ipb --data read -o /dev/null
curl -s --request PUT --user $login:$password https://bitbucket.org/api/1.0/privileges/$login/re216_$unames/jbruneauqueyreix --data read -o /dev/null
curl -s --request PUT --user $login:$password https://bitbucket.org/api/1.0/privileges/$login/re216_$unames/dnegru --data read -o /dev/null

echo "now downloading the source"
git clone https://$login:$password@bitbucket.org/$login/re216_$unames.git
cd re216_$unames
git remote add teachers_repo https://bitbucket.org/re216/re216.git

echo "we are all good, you can start working!"
xdg-open https://bitbucket.org/$login/re216_$unames

./create-project-files.sh
