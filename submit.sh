echo "Ready to submit your work to TA"

current_repo=`git config --get remote.origin.url|sed -rn "s/.*bitbucket.org\/(.*)\.git/\1/p"`

echo "you are submitting your work on $current_repo"
echo "Enter your bitbucket user name"
read login
echo "Great. Now enter your bitbucket password"
read -s password
echo "Super, now enter an optional comment (you can leave this blank)"
read comment
echo "finally, please indicate which teacher is in charge of your group (1) is Daniel Negru, (2) is Joachim Bruneau, (3) is Nicolas Herbaut"
read teacherid


if [[ "$teacherid" = "1" ]] 
then
let teacher="dnegru"
elif [[ "$teacherid" = "2" ]]
then
let teacher="jbruneauqueyreix"
else
let teacher="nherbaut-ipb"
fi




curl 2>>re216.log -o re216-curl.log --silent --write-out '%{http_code}' curl -H "Content-Type: application/json"  -u $login:$password -X POST https://api.bitbucket.org/2.0/repositories/re216/re216/pullrequests/ -d "
{
    \"title\": \"Work Submission\",
    \"description\": \"$comment\",
    \"source\": {
        \"branch\": {
            \"name\": \"master\"
        },
        \"repository\": {
            \"full_name\": \"$current_repo\"
        }
    },
    \"destination\": {
        \"branch\": {
            \"name\": \"master\"
        }
       
     },
    \"reviewers\": [
    ],
    \"close_source_branch\": false
}
"




